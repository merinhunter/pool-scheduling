from django.db import models
from django.forms.models import model_to_dict

# Create your models here.

class Job(models.Model):
    owner = models.CharField(max_length=128)
    birth = models.DateField(auto_now_add=True)
    kind = models.CharField(max_length=12)

    class Meta:
        abstract = False

    def data(self):
        """Return a dictionary with the 'true' fields

        By true fields, we mean those not expressing relations,
        identifiers, etc., that were not in the definition.
        In particular, we need to avoid the OneToOneField
        created by child classes, and the Id field.
        """
        fields = [field.name for field
                  in self._meta.get_fields()
                  if (not isinstance(field, models.OneToOneField) and
                     field.name != 'id')]
        return model_to_dict(self, fields=fields)

    def __str__(self):
        return(str(self.data()))

class JobGit(Job):
    repo = models.CharField(max_length=128)

    def __init__(self, *args, **kwargs):
        kwargs['kind'] = 'git'
        super().__init__(*args, **kwargs)
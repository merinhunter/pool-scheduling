"""
Classes for pools of different kinds of jobs, rooted at the Pool class.
"""

from .models import JobGit as JobGitModel
from .models import Job as JobModel

class Pool:
    """Pool of pending jobs"""

    def __init__(self):
        pass

    def add(self, job):
        """Add job to the pool of pending jobs"""
        pass

    def get_by_owner(self, owner, limit=1):
        """Get job(s) corresponding to an owner

        :param owner: owner of jobs
        :param limit: maximum number of jobs to return
        :return:      list of jobs
        """

        jobs = JobModel.objects.filter(owner=owner) \
                   .order_by('birth')[limit:]
        return(jobs)

class PoolGit(Pool):
    """Pool for git jobs"""

    def __init__(self):
        pass

    def add(self, job):
        job = JobGitModel(repo=job.repo, owner=job.owner)
        job.save()

    def count(self):
        """Return the number of jobs in the pool"""
        return JobGitModel.objects.count()

    def objects(self):
        """Return the 'objects' object for the pool"""
        return JobGitModel.objects

    def get_by_repo(self, repo):
        """Get the job(s) corresponding to a repository """
        return JobGitModel.objects.get(repo=repo)

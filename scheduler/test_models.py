from django.test import TestCase
from .models import Job, JobGit

class TestJob(TestCase):

    def test_simple(self):
        job = Job(owner='owner')
        job.save()
        job2 = Job.objects.get(owner='owner')
        self.assertEqual(job2, job)

    def test_to_dict(self):
        job = Job(owner='owner')
        job.save()
        job2 = Job.objects.get(owner='owner')
        self.assertEqual(job2.data(), {'owner': 'owner',
                                       'kind': ''})


class TestJobGit(TestCase):

    def test_simple(self):
        job = JobGit(owner='owner', repo='repo')
        job.save()
        job2 = JobGit.objects.get(owner='owner')
        self.assertEqual(job2, job)

    def test_to_dict(self):
        job = JobGit(owner='owner', repo='repo')
        job.save()
        job2 = JobGit.objects.get(owner='owner')
        self.assertEqual(job2.data(), {'owner': 'owner',
                                       'kind': 'git',
                                       'repo': 'repo'})

    def test_job_git(self):
        job = JobGit(owner='owner', repo='repo')
        job.save()
        job2 = Job.objects.get(owner='owner')
        self.assertEqual(job2.data(), {'owner': 'owner',
                                       'kind': 'git'})
